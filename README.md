## Atividade Prática 01

O objetivo é criar uma função que gera um número aleatório de 1 a 10 como uma promessa, após 3 segundos. Caso o número seja menor que 5, retorna uma mensagem de sucesso; senão, retorna uma mensagem de erro.

- **Conceitos trabalhados**: Promises, funções, bibliotecas, condicionais e assincronismo.

Para executar, abra o terminal no diretório do projeto e execute: `node index.js`
