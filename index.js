function gerarNumero() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const number = Math.floor(Math.random() * 10) + 1;

      if (number < 5) {
        resolve(number);
      } else {
        reject('O número gerado não é menor que 5.');
      }
    }, 3000);
  });
}

(async () => {
  gerarNumero()
    .then((result) =>
      console.log(`SUCESSO: o número gerado foi ${JSON.stringify(result)}`),
    )
    .catch((error) => console.log(`INVÁLIDO: ${JSON.stringify(error)}`));
})();
